<?php

namespace spec\vnexpress;

use vnexpress\GetData;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GetDataSpec extends ObjectBehavior
{

    function it_returns_empty_for_input_empty_when_construct()
    {
        $this->beConstructedWith('');
        $this->error->shouldEqual('Empty Input');
    }

    function it_returns_not_the_string_for_input_number_when_construct()
    {
        $this->beConstructedWith(2);
        $this->error->shouldEqual('Not the string');
    }

    function it_returns_invalid_url_for_input_invalid_url_when_construct()
    {
        $this->beConstructedWith("http.abx.com");
        $this->error->shouldEqual('Invalid URL');
    }

    // function it_returns_html_format_when_call_getHTML_function()
    // {
    //     $this->beConstructedWith("");
    //     $this->getHTML()->shouldReturn(2);
    // }
}
