<?php

namespace spec\vnexpress;

use vnexpress\Store;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StoreSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Store::class);
    }

    function it_returns_empty_for_input_empty_when_call_saveHTML_function()
    {
        $this->saveHTML('')->shouldReturn(false);
        $this->error->shouldEqual('Empty Input');
    }

    function it_returns_not_the_string_for_input_number_when_call_saveHTML_function()
    {
        $this->saveHTML(2)->shouldReturn(false);
        $this->error->shouldEqual('Not the string');
    }

    // function it_returns_invalid_html_format_for_input_invalid_html_when_call_saveHTML_function()
    // {
        
    // }
}
