<?php
    namespace vnexpress;

    class Store
    {
        public $error;
        public function saveHTML($html)
        {
            if(empty($html)) {
                $this->error = "Empty Input";
                return false;
            }

            if(!is_string($html)) {
                $this->error = "Not the string";
                return false;
            }

            try{

                $file = fopen('crawl/index.html', 'w');
                if (!$file)
                    echo "Open File Fail!";
                else {
                    fwrite($file, $html);
                }

            }catch(Exception $e){

                die("Error: Cann't opent file!");
            }
        }

        public function saveHTMLOfline($htmlOffline)
        {
            if(empty($htmlOffline)) {
                $this->error = "Empty Input";
                return false;
            }

            if(!is_string($htmlOffline)) {
                $this->error = "Not the string";
                return false;
            }

            try{

                $file = fopen('crawl/indexoffline.html', 'w');
                if (!$file)
                    echo "Open File Fail!";
                else {
                    fwrite($file, $htmlOffline);
                }

            }catch(Exception $e){

                die("Error: Cann't opent file!");
            }
        }

        public function saveIMG($matchesIMG)
        {
            $arrayLinkIMG = $matchesIMG;
            foreach($arrayLinkIMG as $url)
            {
                $filename = basename($url);
                file_put_contents('crawl/images/' . $filename, file_get_contents($url));

            }
        }

        public function saveCSS($matchesCSS)
        {
            $arrayLinkCSS = $matchesCSS;
            foreach($arrayLinkCSS as $url)
            {
                $filename = basename($url);
                file_put_contents('crawl/css/' . $filename, file_get_contents($url));

            }
        }

        public function saveJS($matchesJS)
        {
            $arrayLinkJS = $matchesJS;
            foreach($arrayLinkJS as $url)
            {
                $filename = basename($url);
                file_put_contents('crawl/js/' . $filename, file_get_contents($url));

            }
        }
    }

?>