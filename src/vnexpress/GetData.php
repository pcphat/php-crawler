<?php
    namespace vnexpress;

    class GetData
    {
        private $html;
        public $error;
        public function __construct($url)
        {
            if(empty($url)) {
                $this->error = "Empty Input";
                return false;
            }

            if(!is_string($url)) {
                $this->error = "Not the string";
                return false;
            }

            if($this->invalidURL($url) == false) {
                $this->error = "Invalid URL";
                return false;
            }

            $this->html = file_get_contents($url);
        }

        public function getHTML()
        {
            return $this->html;
        }

        public function getIMG()
        {
            $patternIMG = '|<img.*?src=[\'"](.*?)[\'"].*?>|i';
            preg_match_all( $patternIMG, $this->html, $matchesIMG );
            return $matchesIMG[1];
        }

        public function getCSS()
        {
            $patternCSS = '/<link href="(.*)"(.*)rel="stylesheet"(.*)\/>/';
            preg_match_all( $patternCSS ,$this->html, $matchesCSS );
            return $matchesCSS[1];
        }

        public function getJS()
        {
            $patternJS = '/<script.*?src="(.*?)"/';
            preg_match_all( $patternJS ,$this->html, $matchesJS );
            return $matchesJS[1];
        }

        //REFACTOR
        private function invalidURL($url)
        {
            $pattern = "/(http|https|ftp):\\/\\/(www\\.)?[\\w\\-]+\\.[\\w]+(\\.[\\w]+)*\\/?(([\\w\\-]+)\\/?)*([\\w\\-_]+\.(php|html|htm|jsp|aspx))?(\\?([\\w\\-]+=[\\w\\-]+)*(&([\\w\\-]+=[\\w\\-]+))*)?/";
            if (preg_match($pattern, $url))
                return true;
            
            return false;
        }

    }
?>