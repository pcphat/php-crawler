<?php
    namespace vnexpress;

    class Offline
    {
        public $error;

        public $htmlOffline;

        public function __construct($html)
        {
            $this->htmlOffline = $html;
        }
        public function toOffline($listImgUrl, $listCSSUrl, $listJSUrl)
        {

            // try{

            //     $lines = file('index.html');
            //     foreach ($lines as $line) {
            //         if(strpos($line, )) {
            //          $str = str_replace( 'src="https://dantricdn.com/zoom ', '', $str );
            //         }
            //     }

            // }catch(Exception $e){

            //     die("Error: Cann't opent file!");
            // }

            foreach($listImgUrl as $imgLink)
            {
                $filenameImgUrl = basename($imgLink);
                $this->htmlOffline = str_replace( $imgLink, 'images/' . $filenameImgUrl, $this->htmlOffline );
            }

            foreach($listCSSUrl as $cssLink)
            {
                $filenameCssUrl = basename($cssLink);
                $this->htmlOffline = str_replace( $cssLink, 'css/' . $filenameCssUrl, $this->htmlOffline );
            }

            foreach($listJSUrl as $jsLink)
            {
                $filenameJsUrl = basename($jsLink);
                $this->htmlOffline = str_replace( $jsLink, 'js/' . $filenameJsUrl, $this->htmlOffline );
            }

            return $this->htmlOffline;
        }

    }

?>