<?php
    include "vendor/autoload.php";

    // $url = "http://dantri.com.vn/";
    $url = "http://genk.vn/";
    // $url = "https://vnexpress.net/";

    $data = new vnexpress\GetData($url);
    $save = new vnexpress\Store();

    $html = $data->getHTML();
    $arrayIMGLink = $data->getIMG();
    $arrayCSSLink = $data->getCSS();
    $arrayJSLink = $data->getJS();

    //Conver offline site
    $offline = new vnexpress\Offline($html);
    $htmlOffline = $offline->toOffline($arrayIMGLink, $arrayCSSLink, $arrayJSLink);

    $save->saveHTML($html);
    $save->saveHTMLOfline($htmlOffline);
    $save->saveIMG($arrayIMGLink);
    $save->saveCSS($arrayCSSLink);
    $save->saveJS($arrayJSLink);

    print_r($arrayJSLink);


?>